// package br.com.grupo6.apiuser.dao;

// import br.com.grupo6.apiuser.models.User;
// import br.com.grupo6.apiuser.util.UserCreator;
// import org.assertj.core.api.Assertions;
// import org.junit.jupiter.api.DisplayName;
// import org.junit.jupiter.api.Test;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

// import java.util.List;
// import java.util.Optional;

// @DataJpaTest
// @DisplayName("Tests for User DAO")
// public class UserDAOTest {
//     @Autowired
//     private UserDAO dao;

//     @Test
//     @DisplayName("save persist user when successful")
//     void savePersistUserWhenSuccessful(){
//         User userToBeSaved = UserCreator.createUserToBeSaved();
//         User userSaved = dao.save(userToBeSaved);

//         Assertions.assertThat(userSaved).isNotNull();
//         Assertions.assertThat(userSaved.getId()).isNotNull();
//         Assertions.assertThat(userSaved.getName()).isEqualTo(userSaved.getName());
//         Assertions.assertThat(userSaved.getBirth()).isEqualTo(userSaved.getBirth());
//         Assertions.assertThat(userSaved.getPhone()).isEqualTo(userSaved.getPhone());
//         Assertions.assertThat(userSaved.getEmail()).isEqualTo(userSaved.getEmail());
//         Assertions.assertThat(userSaved.getCpf()).isEqualTo(userSaved.getCpf());
//     }

//     @Test
//     @DisplayName("save updates user when successful")
//     void saveUpdatesUserWhenSuccessful(){
//         User userToBeSaved = UserCreator.createUserToBeSaved();
//         User userSaved = dao.save(userToBeSaved);

//         userSaved.setName("Carla");
//         userSaved.setPhone("13 98562589");
//         userSaved.setEmail("carla@test.com");

//         Assertions.assertThat(userSaved).isNotNull();
//         Assertions.assertThat(userSaved.getId()).isNotNull();
//         Assertions.assertThat(userSaved.getName()).isEqualTo(userSaved.getName());
//         Assertions.assertThat(userSaved.getBirth()).isEqualTo(userSaved.getBirth());
//         Assertions.assertThat(userSaved.getPhone()).isEqualTo(userSaved.getPhone());
//         Assertions.assertThat(userSaved.getEmail()).isEqualTo(userSaved.getEmail());
//         Assertions.assertThat(userSaved.getCpf()).isEqualTo(userSaved.getCpf());
//     }

//     @Test
//     @DisplayName("delete user when Successful")
//     void deleteUserWhenSuccessful(){
//         User userToBeSaved = UserCreator.createUserToBeSaved();
//         User userSaved = dao.save(userToBeSaved);

//         dao.delete(userSaved);

//         Optional<User> userOptional = dao.findById(userSaved.getId());
//         Assertions.assertThat(userOptional).isEmpty();
//     }

//     @Test
//     @DisplayName("finds user by Id when successful")
//     void findsUserByIdWhenSuccessful() {
//         User userToBeSaved = UserCreator.createUserToBeSaved();
//         User userSaved = dao.save(userToBeSaved);

//         Optional<User> optionalUser = dao.findById(userSaved.getId());
//         Assertions.assertThat(optionalUser.isPresent()).isTrue();

//         User user = optionalUser.get();

//         Assertions.assertThat(user).isNotNull().isEqualTo(userSaved);
//         Assertions.assertThat(user.getId()).isNotNull().isEqualTo(userSaved.getId());
//         Assertions.assertThat(userSaved.getName()).isEqualTo(userSaved.getName());
//         Assertions.assertThat(userSaved.getBirth()).isEqualTo(userSaved.getBirth());
//         Assertions.assertThat(userSaved.getPhone()).isEqualTo(userSaved.getPhone());
//         Assertions.assertThat(userSaved.getEmail()).isEqualTo(userSaved.getEmail());
//         Assertions.assertThat(userSaved.getCpf()).isEqualTo(userSaved.getCpf());
//     }


// }
