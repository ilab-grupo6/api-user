// package br.com.grupo6.apiuser.controller;

// import br.com.grupo6.apiuser.models.User;
// import br.com.grupo6.apiuser.service.UserService;

// import br.com.grupo6.apiuser.util.UserCreator;
// import org.assertj.core.api.Assertions;
// import org.junit.jupiter.api.BeforeEach;
// import org.junit.jupiter.api.DisplayName;
// import org.junit.jupiter.api.Test;
// import org.junit.jupiter.api.extension.ExtendWith;
// import org.mockito.ArgumentMatchers;
// import org.mockito.BDDMockito;
// import org.mockito.InjectMocks;
// import org.mockito.Mock;
// import org.springframework.data.domain.Page;
// import org.springframework.data.domain.PageImpl;
// import org.springframework.test.context.junit.jupiter.SpringExtension;

// import java.util.List;

// @ExtendWith(SpringExtension.class)
// public class UserControllerTest {
//      @InjectMocks
//      private UserController userController;
//      @Mock
//      private UserService service;

// //    @BeforeEach
// //    void setUp() throws Exception {
// //        PageImpl<User> userPage = new PageImpl<>(List.of(UserCreator.createValidUser()));
// //        BDDMockito.when(service.orderPage(ArgumentMatchers.any()))
// //                .thenReturn(userPage);
// //
// //        BDDMockito.when(service.findAll())
// //                .thenReturn(List.of(UserCreator.createValidUser()));
// //
// //        BDDMockito.when(service.findById(ArgumentMatchers.anyLong()))
// //                .thenReturn(UserCreator.createValidUser());
// //
// //        BDDMockito.when(service.createUser(ArgumentMatchers.any())).thenReturn(UserCreator.createValidUser());
// //
// //        //BDDMockito.doNothing().when(service).updateUser(ArgumentMatchers.any());
// //
// //        //BDDMockito.doNothing().when(service).deleteUser(ArgumentMatchers.anyLong());
// //
// //        BDDMockito.when(service.deleteUser(ArgumentMatchers.anyLong()))
// //                .thenReturn(null);
// //    }
// //
// //    @Test
// //    @DisplayName("Returns page of users when successful")
// //    void returnsPageOfUsersWhenSuccessful() {
// //        User user = UserCreator.createValidUser();
// //        Page<User> orderPage = userController.returnPage(null).getBody();
// //
// //        Assertions.assertThat(orderPage).isNotNull();
// //        Assertions.assertThat(orderPage.toList())
// //                .isNotEmpty()
// //                .hasSize(1);
// //
// //        User returnedUser = orderPage.toList().get(0);
// //
// //        Assertions.assertThat(returnedUser.getCpf()).isEqualTo(user.getCpf());
// //        Assertions.assertThat(returnedUser.getBirth()).isEqualTo(user.getBirth());
// //        Assertions.assertThat(returnedUser.getPhone()).isEqualTo(user.getPhone());
// //        Assertions.assertThat(returnedUser.getEmail()).isEqualTo(user.getEmail());
// //        Assertions.assertThat(returnedUser.getName()).isEqualTo(user.getName());
// //    }
// //
// //    @Test
// //    @DisplayName("returns list of users when successful")
// //    void returnsListOfUsersWhenSuccessful(){
// //        Long expectedId = UserCreator.createValidUser().getId();
// //        List<User> users = userController.getAllUsers().getBody();
// //
// //        Assertions.assertThat(users)
// //                .isNotNull()
// //                .isNotEmpty()
// //                .hasSize(1);
// //        Assertions.assertThat(users.get(0).getName()).isEqualTo(expectedId);
// //    }
// //
// //    @Test
// //    @DisplayName("findById returns anime when successful")
// //    void findByIdReturnsUserWhenSuccessful() throws ResourceNotFoundException {
// //        Long expectedId = UserCreator.createValidUser().getId();
// //        User user = userController.getUserById(1L).getBody();
// //
// //        Assertions.assertThat(user).isNotNull();
// //        Assertions.assertThat(user.getId()).isNotNull().isEqualTo(expectedId);
// //    }
// }
