// package br.com.grupo6.apiuser.service;

// import br.com.grupo6.apiuser.dao.UserDAO;
// import br.com.grupo6.apiuser.exception.ResourceNotFoundException;
// import br.com.grupo6.apiuser.models.User;
// import br.com.grupo6.apiuser.service.UserService;
// import br.com.grupo6.apiuser.util.UserCreator;
// import org.assertj.core.api.Assertions;
// import org.junit.jupiter.api.BeforeEach;
// import org.junit.jupiter.api.DisplayName;
// import org.junit.jupiter.api.Test;
// import org.junit.jupiter.api.extension.ExtendWith;
// import org.mockito.ArgumentMatchers;
// import org.mockito.BDDMockito;
// import org.mockito.InjectMocks;
// import org.mockito.Mock;
// import org.springframework.data.domain.Page;
// import org.springframework.data.domain.PageImpl;
// import org.springframework.data.domain.PageRequest;
// import org.springframework.test.context.junit.jupiter.SpringExtension;

// import java.util.List;
// import java.util.Optional;
// import java.util.UUID;

// @ExtendWith(SpringExtension.class)
// public class UserServiceTest {
//     @InjectMocks
//     private UserService userService;

//     @Mock
//     private UserDAO dao;

//     @BeforeEach
//     void setUp(){
//         PageImpl<User> userPage = new PageImpl<>(List.of(UserCreator.createValidUser()));
//         BDDMockito.when(dao.findAll(ArgumentMatchers.any(PageRequest.class)))
//                 .thenReturn(userPage);

//         BDDMockito.when(dao.findById(ArgumentMatchers.any()))
//                 .thenReturn(Optional.of(UserCreator.createValidUser()));

//         BDDMockito.when(dao.save(ArgumentMatchers.any(User.class)))
//                 .thenReturn(UserCreator.createValidUser());

//         BDDMockito.doNothing().when(dao).delete(ArgumentMatchers.any(User.class));
//     }

//     @Test
//     @DisplayName("Returns page of users when successful")
//     void returnsPageOfUsersWhenSuccessful() {
//         User user = UserCreator.createValidUser();

//         Page<User> userPage = userService.orderPage(PageRequest.of(1,1));

//         Assertions.assertThat(userPage).isNotNull();
//         Assertions.assertThat(userPage.toList())
//                 .isNotEmpty()
//                 .hasSize(1);

//         User returnedUser = userPage.toList().get(0);

//         Assertions.assertThat(returnedUser.getCpf()).isEqualTo(user.getCpf());
//         Assertions.assertThat(returnedUser.getBirth()).isEqualTo(user.getBirth());
//         Assertions.assertThat(returnedUser.getPhone()).isEqualTo(user.getPhone());
//         Assertions.assertThat(returnedUser.getEmail()).isEqualTo(user.getEmail());
//         Assertions.assertThat(returnedUser.getName()).isEqualTo(user.getName());
//     }


//     @Test
//     @DisplayName("findById returns user when successful")
//     void findByIdReturnsUserWhenSuccessful() throws ResourceNotFoundException {
//         UUID expectedId = UserCreator.createValidUser().getId();
//         User user = userService.findById(UUID.fromString("8b9eccb6-2460-4fe1-8faf-55c4f083b33a"));

//         Assertions.assertThat(user).isNotNull();
//         Assertions.assertThat(user.getId()).isNotNull().isEqualTo(expectedId);
//     }

//    @Test
//    @DisplayName("Update user when successful")
//    void updateUserWhenSuccessful() throws Exception {
//        User user = UserCreator.createValidUser();
//        User returnedUser = userService.updateUser(user.getId(), user);
//
//        returnedUser.setName("Carla");
//        returnedUser.setPhone("13 98562589");
//        returnedUser.setEmail("carla@test.com");
//
//        Assertions.assertThat(returnedUser.getPhone()).isEqualTo(user.getPhone());
//        Assertions.assertThat(returnedUser.getEmail()).isEqualTo(user.getEmail());
//        Assertions.assertThat(returnedUser.getName()).isEqualTo(user.getName());
//
//    }

//    @Test
//    @DisplayName("delete user when successful")
//    void deleteUserWhenSuccessful(){
//        Assertions.assertThatCode(() -> userService.deleteUser())
//                .doesNotThrowAnyException();
//    }




// }
