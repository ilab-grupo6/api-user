package br.com.grupo6.apiuser.controller;

import br.com.grupo6.apiuser.exception.ConstraintException;
import br.com.grupo6.apiuser.exception.ResourceNotFoundException;
import br.com.grupo6.apiuser.models.User;
import br.com.grupo6.apiuser.service.UserService;
import br.com.grupo6.apiuser.validation.PostValidation;
import br.com.grupo6.apiuser.validation.PutValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/users")
@CrossOrigin("*")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<Page<User>> returnPage(Pageable pageable) {
        return ResponseEntity.ok(userService.orderPage(pageable));
    }

    @GetMapping("/{id}")
    public ResponseEntity<User> getUserById(@PathVariable(value = "id") UUID userId) throws ResourceNotFoundException {
        User user = userService.findById(userId);
        return ResponseEntity.ok().body(user);
    }

    @GetMapping("/find")
    public ResponseEntity<Optional<List<User>>> getUserByName(@RequestParam(name = "name") String name) throws Exception, ResourceNotFoundException {
        Optional<List<User>> user = userService.findByName(name);
        return ResponseEntity.ok().body(user);
    }

    @PostMapping("/register") 
    @ResponseStatus(HttpStatus.CREATED)
    public User createUser(@Validated(PostValidation.class) @RequestBody User user, BindingResult br) throws ConstraintException, Exception {
        if (br.hasErrors()){
            throw new ConstraintException(br.getAllErrors().get(0).getDefaultMessage());}
        return userService.createUser(user);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<User> updateUser(@PathVariable(value = "id") UUID userId, @Validated(PutValidation.class) @RequestBody User userDetails, BindingResult br) throws ResourceNotFoundException, ConstraintException, Exception {

        if (br.hasErrors()){
            throw new ConstraintException(br.getAllErrors().get(0).getDefaultMessage());}

        User user = userService.updateUser(userId, userDetails);
        return ResponseEntity.ok(user);
    }

    @DeleteMapping("/delete/{id}")
    public Map<String, Boolean> deleteUser(@PathVariable(value = "id") UUID userId) throws ResourceNotFoundException{
        return userService.deleteUser(userId);
    }
}
