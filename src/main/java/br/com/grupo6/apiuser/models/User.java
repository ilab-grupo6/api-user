package br.com.grupo6.apiuser.models;
import javax.persistence.*;
import javax.persistence.Entity;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import br.com.grupo6.apiuser.validation.PostValidation;
import br.com.grupo6.apiuser.validation.PutValidation;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.br.CPF;

import java.util.UUID;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Table(name = "user_tb")
public class User {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id")
    private UUID id;

    @Column(name = "name")
    @NotBlank(message = "Name is required", groups = PostValidation.class)
    private String name;

    @Column(name = "cpf", unique = true)
    @NotBlank(message = "CPF is required", groups = PostValidation.class)
	@CPF(message = "CPF should be valid", groups = PostValidation.class)
    private String cpf;

    @Column(name = "phone")
    @NotBlank(message = "Phone is required", groups = PostValidation.class)
    private String phone;

    @Column(name = "birth")
    @NotBlank(message = "Birth is required", groups = PostValidation.class)
    private String birth;

    @Column(name = "email", unique = true)
	@NotBlank(message = "Email is required", groups = PostValidation.class)
	@Email(message = "Email should be valid", groups = {PostValidation.class, PutValidation.class})
    private String email;
}

