package br.com.grupo6.apiuser.h2;

import br.com.grupo6.apiuser.dao.UserDAO;
import br.com.grupo6.apiuser.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.util.Arrays;

@Configuration
@Profile("test")
public class TestConfig implements CommandLineRunner {
    @Autowired
    UserDAO userDao;

    @Override
    public void run(String... args) throws Exception {

        User user1 = new User(null, "Maria", "750.480.250-67", "11 963258963", "12-02-1990", "maria@email.com");
        User user2 = new User(null, "Carla", "766.473.220-43", "11 963258741", "14-02-1988", "carla@email.com");

        userDao.saveAll(Arrays.asList(user1, user2));
    }
}
