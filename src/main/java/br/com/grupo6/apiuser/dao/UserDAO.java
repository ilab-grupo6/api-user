package br.com.grupo6.apiuser.dao;

import br.com.grupo6.apiuser.exception.ResourceNotFoundException;
import br.com.grupo6.apiuser.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface UserDAO extends JpaRepository <User, UUID> {
    public Optional<List<User>> findByNameContainsIgnoreCase(String name);
}
