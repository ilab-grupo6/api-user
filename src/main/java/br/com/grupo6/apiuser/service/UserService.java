package br.com.grupo6.apiuser.service;

import br.com.grupo6.apiuser.dao.UserDAO;
import br.com.grupo6.apiuser.exception.ConstraintException;
import br.com.grupo6.apiuser.exception.ResourceNotFoundException;
import br.com.grupo6.apiuser.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {
    @Autowired
    private UserDAO userDao;

    public Page<User> orderPage(Pageable pageable){
        return userDao.findAll(pageable);
    }

    public Optional<List<User>> findByName(String name) throws ResourceNotFoundException {
         return Optional.ofNullable(userDao.findByNameContainsIgnoreCase(name))
                 .orElseThrow(() -> new ResourceNotFoundException("User not found for this name: " + name));
    }

    public User findById(UUID userId) throws ResourceNotFoundException {
        User user = userDao.findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException("User not found for this id: " + userId));
        return user;
    }

    public User createUser(User user) throws ConstraintException, Exception {
        try {
            return userDao.save(user);
        }
        catch(DataIntegrityViolationException e) {
            throw new ConstraintException("Constraint Problem:" + e.getMostSpecificCause().getMessage());
        }
        catch(Exception e) {
            throw new Exception("Unknown error: " + e.getMessage());
        }
    }

    public User updateUser(UUID userId, User userDetails) throws Exception {
        User user = userDao.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User not found for this id: " + userId));

        try {
            if(userDetails.getCpf() != null) {
                throw new DataIntegrityViolationException("CPF can not be updated.");
            }
            if(userDetails.getEmail() != null && userDetails.getEmail() != "") {
                user.setEmail(userDetails.getEmail());
            }
            if(userDetails.getName() != null && userDetails.getName() != "") {
                user.setName(userDetails.getName());
            }
            if(userDetails.getBirth() != null) {
                user.setBirth(userDetails.getBirth());
            }

            return userDao.save(user);
        }
        catch(DataIntegrityViolationException e) {
            throw new ConstraintException("Constraint Problem:" + e.getMostSpecificCause().getMessage());
        }
        catch(Exception e) {
            throw new Exception("Unknown error: " + e.getMessage());
        }
    }

    public Map<String, Boolean> deleteUser(UUID userId) throws ResourceNotFoundException{
        User user = userDao.findById(userId).orElseThrow(() -> new ResourceNotFoundException("User not found for this id:" + userId));
        userDao.delete(user);

        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);

        return response;
    }
}
