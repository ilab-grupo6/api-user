# API USER

**A partir daqui, todas os endpoints exigem autenticação.**

#### - GET: /users?page=0&size=1

Este endpoint fornece os dados com uma paginação, onde a página e o tamanho da mesma devem ser informados na URL como mostra o exemplo acima. 
Caso não sejam informados, o padrão é retornar a primeira página com o seu tamanho limitado a 10 elementos.

##### Resposta:

###### Em caso de sucesso, a seguinte resposta será obtida (código `200`):

```json
{
  "content": [
    {
      "id": "8b9eccb6-2460-4fe1-8faf-55c4f083b33a",
      "name": "Maria",
      "cpf": "750.480.250-67",
      "phone": "11 963258963",
      "birth": "12-02-1990",
      "email": "maria@email.com"
    }
  ],
  "pageable": {},
  "totalPages": 3,
  "totalElements": 3,
  "last": false,
  "size": 1,
  "number": 0,
  "sort": {},
  "numberOfElements": 1,
  "first": true,
  "empty": false
}
```

#### - GET: /users/{id}

Este endpoint retorna o usuário do id informado na URL.

##### Resposta:

###### Em caso de sucesso, a seguinte resposta será obtida (código `200`):

```json
{
  "id": "8b9eccb6-2460-4fe1-8faf-55c4f083b33a",
  "name": "Maria",
  "cpf": "750.480.250-67",
  "phone": "11 963258963",
  "birth": "12-02-1990",
  "email": "maria@email.com"
}
```

#### - GET: /users/find?name=NomeDoUsuario

Este endpoint retorna o usuário pelo nome informado nos parâmetros.

##### Parâmetros:

```json
"Key": "name"
"Value": "Maria"
```
##### Resposta:

###### Em caso de sucesso, a seguinte resposta será obtida (código `200`):

```json
{
  "id": "8b9eccb6-2460-4fe1-8faf-55c4f083b33a",
  "name": "Maria",
  "cpf": "750.480.250-67",
  "phone": "11 963258963",
  "birth": "12-02-1990",
  "email": "maria@email.com"
}
```

#### - POST: /users/register

Este endpoint cria um usuário.

##### Requisição:


```json

{
  "name": "Vitor",
  "cpf": "667.663.010-90",
  "phone": "11 963251233",
  "birth": "17-02-1980",
  "email": "vitor@email.com"
}

```

###### Em caso de sucesso, a seguinte resposta será obtida (código `201`): 

```json
{
  "id": "9b9eccb6-2460-4fe1-8faf-55c4f083b33a",
  "name": "Vitor",
  "cpf": "667.663.010-90",
  "phone": "11 963251233",
  "birth": "17-02-1980",
  "email": "vitor@email.com"
}
```

#### - PUT: /users/update/{id}

Este endpoint atualiza os dados de um usuário

##### Requisição:


```json
{
  "phone": "11 967851233",
  "birth": "18-02-1980"
}
```

###### Em caso de sucesso, a seguinte resposta será obtida (código `200`):

```json
{
  "id": "9b9eccb6-2460-4fe1-8faf-55c4f083b33a",
  "name": "Vitor",
  "cpf": "667.663.010-90",
  "phone": "11 963251233",
  "birth": "18-02-1980",
  "email": "vitor@email.com"
}
```

#### - DELETE: /users/delete/{id}

Este endpoint deleta um usuário

##### Resposta:

###### Em caso de sucesso, a seguinte resposta será obtida (código `200`):

```json
{
  "deleted": true
}
```

Considerações:

*O CPF deve ser válido e autêntico;

*Não é possível atualizar o CPF, pois o mesmo é único;

### OBS: 

Todas essas rotas estarão protegidas e necessitam de um token que será validado pela API de admin a cada requisição.
